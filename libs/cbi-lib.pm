#!/usr/bin/perl

use strict;

use HTML::Template;
#use Session;
use CGI::Cookie;
use CGI;
use POSIX;

our $base_dir;
our %config;
our $cgi = new CGI ("");
#our $core_flag; # This flag must been set in core main script
our %in;
our $admin;
our @limits = (0,0,0);

require '../conf/config.conf';

our $admin = (defined $ENV{USER_ID} && $ENV{USER_ID} < 1000) ? 1 : 0;

sub print_header
{
	my $cookie = shift;
	my $url = shift;

	print "Cache-Control: no-cache\n";

	if (defined($cookie))
	{ 	# Set cookies and redirect
		$url = '/' if not defined $url;
		print $cgi->header(-cookie=>$cookie,-location=>"$url",-status=>302);
	}
	elsif (defined $url)
	{ # redirect only
		print $cgi->header(-location=>"$url",-status=>302);
	}
	else
	{
		print $cgi->header(-type=>'text/html',-charset=>$config{'charset'});
	}
}

sub print_header_export
{
	print "Cache-Control: no-cache\n";
	print $cgi->header(-type=>'application/octet-stream',-attachment=>'traffic.txt');
}

sub create_cookie
{
    	my $name = shift;
    	my $value = shift;

	my $cookie = new CGI::Cookie(
                -name => $name,
                -value => $value,
		-expires =>  '+1M',
                -secure => $config{'ssl'}
        );
	
	return $cookie;
}


sub delete_cookie
{
        my $session_id = $_[0];
	my $status = $_[1];

        my $cookie = new CGI::Cookie(
                -name => 'session_id',
                -value => "$session_id",
                -expires => '-1d',
                -secure => $config{'ssl'}
        );
	
	print $cgi->header(-cookie=>$cookie,-location=>"/login.shtml?status=$status",-status=>302);
}

sub shm_auth
{
	my ($user,$pass,$sock) = @_;

	$ENV{SHM_USER} = &encode(&unescape($user));
        $ENV{SHM_PASS} = &encode(&unescape($pass));
	$ENV{SHM_SOCK} = $sock;
	$ENV{SHM_CMD} = 'auth';

	return 0 if -S "$config{socks_path}/$ENV{SHM_SOCK}";

	#my @args = ("$config{perl}",CLI_BIN);
	#chdir($config{socks_path});
	#my ($ret_status,$stdout,$stderr) = &exec_proc(\@args,'',10);
	#warn "[$ret_status, $stdout, $stderr]\n";
	#return $ret_status;

	if (fork()==0)
	{
		chdir($config{socks_path});
		exec($config{shm_cli_bin}) or die "Failed to exec CLI : $!\n";
	}

	wait;

	return $?;
}

sub unescape
{
        my $tmp = shift;
        $tmp =~ s/&amp;/&/g;
        $tmp =~ s/&lt;/</g;
        $tmp =~ s/&gt;/>/g;
        $tmp =~ s/&quot;/\"/g;
        $tmp =~ s/&#(\d+);/chr($1)/ge;

        return $tmp;
}

sub shm_reg
{
	my ($user,$pass,$type) = @_;


	$ENV{SHM_USER} = $user;
	$ENV{SHM_PASS} = $pass;
	$ENV{SHM_TYPE} = $type;
	#$ENV{SHM_SOCK} = $sock;
	$ENV{SHM_CMD} = 'reg';

	return 0 if -S "$config{socks_path}/$ENV{SHM_SOCK}";
	
	if (fork()==0)
	{
		chdir($config{socks_path});
		exec($config{shm_cli_bin}) or die "Failed to exec CLI : $!\n";
	}

	wait;

	return $?;
}

sub send_command
{
	my $cmd = shift;
	my $parse_headers = shift;

	die 'SOCK not defined' if not defined($ENV{SHM_SOCK});

	use IO::Socket;

	#exit 0 if not -f "$config{socks_path}/$ENV{SHM_SOCK}";

	my $sock = IO::Socket::UNIX->new("$config{socks_path}/$ENV{SHM_SOCK}") or die "SOCK: $config{socks_path}/$ENV{SHM_SOCK} $!";

	my @commands = split(/\n/,$cmd);	

	if ($ENV{SUDO})
	{
		@commands = map("{$ENV{SUDO}}$_",@commands);
	}

	print $sock "$_\n" for @commands;
	
	$sock->shutdown(1);

	# Read answer
	my @data;

	#binmode($sock);

	if ($parse_headers)
	{
		# Get headers
		while (<$sock>)
		{
			chomp;chop;
			last if $_ eq '';
		
			if ($_=~/^(.+):\s+(.+)$/)
			{
				$ENV{"SHM-$1"} = $2;
			}
		}
	}

	#if ($ENV{"SHM-charset"} eq 'binary')
	#{
	#	open TEST, ">/home/clients/websites/w_vipb/bill.biit.ru/public_html/file.bin" or die $!;	
	#	print TEST $_ while (<$sock>);
	#	close TEST;
	#}

	# Get data
	while (<$sock>)
	{
		#chomp;chop;
		push @data, $_;
	}

	die "$cmd: permission denied" if $data[0]=~/Error: command not found/i;

	if ($data[$#data] =~/^LIMITS:(\d+),(\d+),(\d+)$/)
	{
		pop @data;
		@limits = ($1,$2,$3);
	}

	return @data;
}

sub encode { unpack 'H*',shift };
sub decode { pack 'H*',shift };

# Escape HTML data for HASH, ARRAY and SCALAR
# usage: &escape(\%HASH); &escape(\@ARR); &escape(\$string);
sub escape
{
	my $data = shift;

	if (ref $data eq 'HASH')
	{
		for (keys %{$data})
		{
			$data->{$_} = $cgi->escapeHTML(&trim($data->{$_})); 
		}
	}
	elsif (ref $data eq 'ARRAY')
	{
		for (@{$data})
		{
			$data->[$_] = $cgi->escapeHTML(&trim($data->[$_])); 
		}
	}
	elsif (ref $data eq 'SCALAR')
	{
		${$data} = $cgi->escapeHTML(&trim(${$data}));
	}
	else
	{
		die ("Type of variable not supported");
	}
}

sub quote_hash
{
        return undef if ref($_[0]) ne 'HASH';

        my $buff;
        $buff .= "$_=". &encode($_[0]->{$_}) . ";" for (keys %{$_[0]});
        return $buff;
}

# Conver array to unquoted hash
sub get_hash
{
        my $str = shift;

	my @data = split(/;/,$str);

        my %hash;

        for (@data)
        {
                chomp;

                if (/^([A-Za-z0-9_]+)=([A-Za-z0-9]+)$/)
		{
                        $hash{$1} = &decode($2);
		}
        }
        return %hash;
}

sub get_date
{
	my $date=shift;
	
	my ($day,$month,$year) = (localtime($date))[3,4,5]; 
	my $today = sprintf("%02d.%02d.%04d", $day, $month + 1, $year + 1900);
	return $today;
}

sub get_full_date
{
	my $date=shift;
	
	my ($sec,$min,$hour,$day,$month,$year) = (localtime($date))[0,1,2,3,4,5]; 
	my $today = sprintf("%02d.%02d.%04d %02d:%02d:%02d", $day, $month + 1, $year + 1900,$sec,$min,$hour);
	return $today;
}

sub get_string_date
{
        my $date = shift;

        my ($sec,$min,$hour,$day,$month,$year) = (localtime($date))[0,1,2,3,4,5];
        $year += 1900;

        my @months = ('������','�������','�����','������','���','����','����','�������','��������','�������','������','�������');
        return sprintf("%02d %s %04d %02d:%02d:%02d",$day,$months[$month],$year,$hour,$min,$sec);
}


sub trim { my $str = shift; $str=~s/^\s+|\s+$//g; $str };

# Execute module with perl
sub exec_module
{
        my @params      = @{$_[0]};
        my $stdin       = $_[1];
        my $time_out    = $_[2] || $config{'time_out'};

        my @args = ("$config{perl}");

        for (0..$#params)
        {
                push @args,$params[$_];
        }

        my ($ret_code,$stdout,$stderr) = &exec_proc(\@args,$stdin,$time_out);

        return ($ret_code,$stdout,$stderr);
}

# Wait run script and read returned data. For example:
# my ($ret_status,$stdout,$stderr) = &exec_proc(\@args,$stdin,$time_out);
sub exec_proc
{
        my ($cmd,$stdin,$time_out) = @_;

        my @args = @{$cmd};

        $! = 1;
        pipe(CGIINr, CGIINw); pipe(CGIOUTr, CGIOUTw); pipe(CGIERRr, CGIERRw);

        my $cgipid;

        if (!($cgipid = fork()))
        {
		POSIX::setsid();
		open(STDIN, "<&CGIINr"); open(STDOUT, ">&CGIOUTw"); open(STDERR, ">&CGIERRw");
                close(CGIINw); close(CGIOUTr); close(CGIERRr);
		#chdir($config{'modules_path'}) or die "Can't chdir to modules directory";
                exec { $args[0] } @args  or die "Failed to exec <$args[0]> : $!\n";
        }
        close(CGIINr); close(CGIOUTw); close(CGIERRw);

        # Set timer
        alarm($time_out); local $SIG{ALRM} = sub { kill 9, $cgipid };

        # Put to stdin child
        print CGIINw $stdin if defined($stdin);
        close(CGIINw);

        # wait for child
        waitpid($cgipid,0); my $ret_status = $?;
        # Unset timer
        alarm(0);

	# Read child stdout and stderr
        my $stdout; $stdout .= $_ while(<CGIOUTr>);
        my $stderr; $stderr .= $_ while(<CGIERRr>);

        close(CGIOUTr); close(CGIERRr);

        return ($ret_status,$stdout,$stderr);
}

sub get_full_date
{
	my $date=shift;

	my ($sec,$min,$hour,$day,$month,$year) = (localtime($date))[0,1,2,3,4,5];
	my $today = sprintf("%02d.%02d.%04d %02d:%02d:%02d", $day, $month + 1, $year + 1900,$hour,$min,$sec);
	return $today;
}

sub passgen
{
	my $length = shift || 8;

	my @chars =('e','r','t','p','a','d','f','h','k','z','x','c','b','n','m', 'E','R','T','P','A','D','F','H','K','Z','X','C','B','N','M', 1 .. 9);
	my $pass = join("", @chars[ map { rand @chars } (1 .. $length) ]);
	return $pass;
}


sub create_password
{
        return &passgen();
}

sub get_full_date
{
	my $date=shift;

	my ($sec,$min,$hour,$day,$month,$year) = (localtime($date))[0,1,2,3,4,5];
	my $today = sprintf("%02d.%02d.%04d %02d:%02d:%02d", $day, $month + 1, $year + 1900,$hour,$min,$sec);
	return $today;
}

sub get_unixtime_start
{
        return &get_unixtime(   $in{sec_start},
                                $in{min_start},
                                $in{hour_start},
                                $in{day_start},
                                $in{month_start},
                                $in{year_start} );
}


sub get_unixtime_stop
{
        return &get_unixtime(   $in{sec_stop},
                                $in{min_stop},
                                $in{hour_stop},
                                $in{day_stop},
                                $in{month_stop},
                                $in{year_stop}  );
}

sub get_unixtime
{
        my $sec = shift;
        my $min = shift;
        my $hour = shift;
        my $day = shift;
        my $month = shift;
        my $year = shift;

        use Time::Local 'timelocal_nocheck';

        my $s_time = timelocal_nocheck($sec,$min,$hour,$day,$month,$year);
        return $s_time;
}

sub get_period_string
{
	my $data = shift;
	my $period;

	if ($data =~/^\d+$/)
	{
		$period = "$data ���.";
	}
	elsif ($data =~/^(\d+)\.(\d+)$/)
	{
		# 0.01 - 0.09 = 1-9 days
		# 0.1  - 0.9  = 10,20,30,...,90 days
		# 0.11 - 0.99 = 11,12,13,...,31,32,...,99 days
		
		my $days = $2;
		$period = "$1 ���. " . ($days=~/^0/ ? int($days) : ($days < 10 ? $days*10 : $days) ) . " ����.";
	}
	else
	{
		return "-";
	}

	return $period;	
}

sub check_captcha
{
	my $code = shift;

	my $file = "/tmp/$ENV{REMOTE_ADDR}";

	if (-f $file)
	{
		open FD, "<$file" or die $!;
		my $word = <FD>;
		close FD;
		unlink $file;

		return 1 if (lc($code) eq lc($word));
	}
	return 0;
}

sub start_ftp
{
	my $session_id = shift;
	my $params = shift;

	my $host = $params->{server}->{server_name};
	my $login = $params->{login};
	my $password = $params->{password};
	my $dir = "$base_dir/user_sshfs";

	sub check_mount
	{
		my $wait_sec = shift;

		my ($dgid, $duid);

		for (0..$wait_sec)
		{
			($duid, $dgid) = (stat("$dir/$session_id/$login"))[4,5];

			if (length $duid and length $dgid)
			{
				return 1 if $duid != 1002 and $dgid != 155;
			}
			sleep 1;
		}
		
		return 0;
	}

	if (-d "$dir/$session_id/$login")
	{
		return 1 if &check_mount(0);
	}

	pipe (PARENTR, PARENTW);

	defined (my $child = fork) or return 0;
	
	if (!$child)
	{
		close PARENTW;
		
		chdir $dir or die $!;
		mkdir $session_id unless -d $session_id;
		mkdir "$session_id/$login" unless -d "$session_id/$login";

		POSIX::setsid() or die "setsid failed: $!";

		STDIN->fdopen(*PARENTR,"<") or die "STDIN: $!";

		#print 'sshfs', "$login\@$host:", "$session_id/$login", '-o', 'password_stdin,follow_symlinks,UserKnownHostsFile=/dev/null,StrictHostKeyChecking=no';

		#open TEST, ">>/home/clients/websites/w_vipb/bill.biit.ru/ftp.log";
		#print TEST "sshfs $login\@$host: $session_id/$login -o password_stdin,follow_symlinks,UserKnownHostsFile=/dev/null,StrictHostKeyChecking=no\n";
		#close TEST;

		my @args = ('sshfs', "$login\@$host:", "$session_id/$login", '-o', 'password_stdin,follow_symlinks,UserKnownHostsFile=/dev/null,StrictHostKeyChecking=no');

		exec { $args[0] } @args;

		die "ERROR: can't exec ($!)\n";
	}

	close PARENTR;
	
	print PARENTW "$password\n\n";

	close PARENTW;
	
	return &check_mount(3);
}

sub start_mysql
{
	my $session_id = shift;
	my $params = shift;

	return 1;

	return 0 if not $session_id;

	#die "$params->{server}->{ip} port=$params->{port} db=mysql user=root password=$params->{rootpassword}";

	my $dir = "$base_dir/public_html/user_mysql";

	if (-l "$dir/$session_id.fcgi")
	{
		`pgrep -f $dir/$session_id.fcgi`;
		return 1 if $? == 0;
	}
	else
	{
		symlink "$base_dir/public_html/modules/mysql/start_mysql", "$dir/$session_id.fcgi" or return 0;
	}

	my $res = `curl -k $config{url}/user_mysql/$session_id.fcgi?host=$params->{server}->{ip}\\&port=$params->{port}\\&db=mysql\\&user=root\\&password=$params->{rootpassword}`;
	chomp $res;

	return $res eq 'OK' ? 1 : 0;
}

#return 1;
1;


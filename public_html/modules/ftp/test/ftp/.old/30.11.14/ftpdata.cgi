#!/usr/bin/perl

use JSON;
use CGI;
use IO::Socket;
use CGI::Cookie;
use File::Copy;

use strict;

# TODO: check session and IP

my %data;
my @dir;

open (STDERR, ">>/home/clients/websites/w_vipb/bill.biit.ru/public_html/modules/ftp/log.log") or die $!;

my %mime = ( 	'htm' => 'html','html' => 'html',
		'php' => 'php',	'php3'=> 'php',
		'gif' => 'image','tiff' => 'image','png' => 'image','bmp' => 'image','jpg' => 'image','jpeg' => 'image','ico' => 'image','wmf' => 'image',
		'gz' => 'arch','gzip' => 'arch','zip' => 'arch','tar' => 'arch','tgz' => 'arch','rar' => 'arch',
		'wav' => 'audio','mp3' => 'audio','flac' => 'audio','wma' => 'audio',
		'mpeg' => 'video','mpg' => 'video','avi' => 'video','mkv' => 'video',
		'pdf' => 'pdf',
		'doc' => 'word', # TODO: add all extensions
		'xls' => 'excel', # TODO: add all extensions
		'pl' => 'default', # TODO: add icon
		'cgi' => 'default', # TODO: add icon
	#	'css'	=> 'css',
	#	'js'	=> 'js',
	);

#my $args = &parse_query_string();
#my $session_id = $args->{session_id}; # for hook_upload

our ($u_fd, $u_filename);
my $cgi = new CGI(\&hook_upload, undef, 0);

print "Access-Control-Allow-Origin: *\n";

if($ENV{'REQUEST_METHOD'} eq 'OPTIONS')
{
	print $cgi->header(-charset=>'UTF8'); #, 'Access-Control-Allow-Origin' => '*');
	exit 0;
}

#print $cgi->header(-charset=>'UTF8');
#print keys %in;
#print "TEST";
#exit 0;

my %in = $cgi->Vars;

my %cookies = fetch CGI::Cookie;
my $session_id = defined $cookies{session_id} ? $cookies{session_id}->value : (exists $in{session_id} ? $in{session_id} : &parse_query_string()->{session_id});

my $path = exists $in{path} ? $in{path} : &parse_query_string()->{path};
$path=~s/^\///;
$path=~s/\.{2,}//; #cut off '..'

#warn "id: [$session_id], path: [$path]\n";

die "ERROR: wrong session_id\n" if length $session_id != 32;

chdir "/home/clients/websites/w_vipb/bill.biit.ru/user_sshfs/$session_id" or die $!;
#chdir "/home/clients/websites/w_vipb/";


if ($ENV{'REQUEST_METHOD'} eq 'DELETE')
{ # delete
	print $cgi->header(-charset=>'UTF8', -type => 'application/json', -status=> '200');

	if (-f $path)
	{
		unlink $path;
	}
	elsif (-d $path)
	{
		rmdir $path;
	}

	exit 0;
}
elsif ($ENV{'REQUEST_METHOD'} eq 'PUT')
{ # mkdir, mkfile, rename
	print $cgi->header(-charset=>'UTF8', -type => 'application/json', -status=> '200');

	my %ret;

	if ($in{action} eq 'mkdir')
	{


	}
	elsif ($in{action} eq 'mkfile')
	{


	
	}
	elsif ($in{action} eq 'rename')
	{
		my $path = substr($in{path},0, length($in{path})-length($in{name}));
		$path = '' if $path eq $in{path};

		my $file = "$path$in{name}";
		my $newfile = "$path$in{newname}";

		#$file=~s/^\///;
		#$newfile=~s/^\///;

		#print "$path\n$file\n$newfile\n";
	
		if (rename $file, $newfile)
		{
			%ret = ( path=> $newfile, name=> $in{newname}, mime=> &get_mime($newfile) );
		}
	}
	print to_json(\%ret) . "\n";

	exit 0;
}
elsif ($ENV{'REQUEST_METHOD'} eq 'POST' and $in{'uploadedfiles[]'})
{ # upload file[s]
	print $cgi->header(-charset=>'UTF8', -type => 'application/json', -status=> '200');

	my @childrens;
	my %data = (path => "$path", children => \@childrens);

	my @filename = $cgi->param('uploadedfiles[]');

	for (0..scalar(@filename)-1)
	{
		push @childrens, {	path => "$path/$filename[$_]", 
					name => "$filename[$_]",
					mime => &get_mime($filename[$_]),
				};
	}
	print to_json(\%data, {pretty => 1}) . "\n";

	exit 0;
}
elsif ($ENV{'REQUEST_METHOD'} eq 'GET' and exists $in{download})
{ # download file/directory
	my $file = $in{download}; $file=~s/^.*\///;

	if(-f $in{download})
	{
		my $size = (stat($in{download}))[7];

		print $cgi->header(-charset=>'binary', -type => 'application/octet-stream', -content_length => $size, -attachment => $file);

		# TODO: use sysread or read instead string based output (print)
		open FD, "$in{download}" or die $!;
		while (<FD>)
		{
			print $_;
		}
		close FD;
	}
	elsif (-d $in{download})
	{
		print $cgi->header(-charset=>'binary', -type => 'application/octet-stream', -attachment => "$file.tgz");

		print `/bin/tar -czf - $in{download}`;
	}

	exit 0;
}
elsif ($ENV{'REQUEST_METHOD'} eq 'GET' and $ENV{'CONTENT_TYPE'} eq 'application/x-www-form-urlencoded' and exists $in{get})
{ # show/edit file
	print $cgi->header(	-charset=> exists $in{charset} ? $in{charset} : 'UTF8',
	       			-type => 'text/html');

	exit 0 if not -f "$in{get}";

	my $size = (stat($in{get}))[7];
	my $mime = `/usr/bin/file -b --mime-type $in{get}`; chomp($mime);

	if ($mime=~/^text\// or $mime eq 'inode/x-empty')
	{
		if ($size > 1048576) # 1M
		{
			print "Error: file very big: $size\n";
			exit 0;
		}

		open FD, "$in{get}" or die $!;
		while (<FD>)
		{
			print $_;
		}
		close FD;
	}
	else
	{
		print "Error: Not supported file format [$mime]\n";
	}
}
elsif ($ENV{'REQUEST_METHOD'} eq 'GET') # and $ENV{'CONTENT_TYPE'} eq 'application/json')
{ # get tree
	print $cgi->header(-charset=>'UTF8', -type => 'application/json');

	my (@childrens, %data);

	# Get root
	#opendir(my $dh, $path ? "$path" : ".") or die $!;
	#my @root = grep { !/^\.{1,2}/ } readdir($dh);
	#close $dh;

	#$path .= "/$root[0]";
	#chdir  $path;

	my $idx;

	%data = $path ? (path => "$path", children => \@childrens) : 
			( identifier => 'path', label => 'name', items => [{path => '', name => 'root', mime => 'folder', children => \@childrens}]);

	$idx = 15;

	opendir(my $dh, $path ? "$path" : ".") or die $!;
	#my @files = grep { !/^\.{1,2}/ } readdir($dh);
	#for (sort {$b cmp $a} @files)
	while(readdir $dh)
	{
		next if /^\.{1,2}/;
		
		my %file = ( name => $_, path => ($path ? "$path/$_" : "$_") );

		if (-d ($path ? "$path/$file{name}" : $file{name}) )
		{
			$file{children} = [];
			$file{mime} = 'folder';
		}
		else
		{
			$file{mime} = &get_mime($file{name});
		}
		push @childrens, \%file;
	}
	closedir $dh;

	@childrens = sort {$a->{name} <=> $b->{name}} @childrens;

	@childrens = sort {&up_folders($a->{mime}) <=> &up_folders($b->{mime})} @childrens;

	print to_json(\%data, {pretty => 1}) . "\n";
}
else
{
	print $cgi->header(-charset=>'UTF8', -type => 'text/html');
	print "Unknown action\n";
}


exit 0;

sub get_mime
{
	my $name = shift;

	#my $mime = `/usr/bin/file -b --mime-type $in{get}`; chomp($mime);

	$name=~s/.*\.//;
	return exists $mime{lc($name)} ? $mime{lc($name)} : 'default';

}

sub up_folders { $_[0] eq 'folder' ? 0 : 1; }

sub hook_upload
{
	my ($filename, $buffer, $bytes_read) = @_;


	if ($filename ne $u_filename)
	{
		my $args = &parse_query_string();
		my $session_id = $args->{session_id};
		die "ERROR: wrong session_id\n" if length $session_id != 32;

		my $path = $args->{path};
		$path=~s/^\///; $path=~s/\.{2,}//; #cut off '/' and '..'

		close $u_fd if defined $u_fd;

		open ($u_fd, ">/home/clients/websites/w_vipb/bill.biit.ru/user_sshfs/$session_id/$path/$filename") or die $!;
		
		binmode $u_fd;
	
		$u_filename = $filename;
	}

	print $u_fd $buffer;

	return;
}

sub parse_query_string
{
	my %ret;

	if ($ENV{'QUERY_STRING'}=~/=/)
	{
	        my @in = split(/\&/, $ENV{'QUERY_STRING'});
	
        	my $i;

	        foreach $i (@in)
        	{
	                my ($k, $v) = split(/=/, $i, 2);
        	        $k =~ s/\+/ /g; $k =~ s/%(..)/pack("c",hex($1))/ge;
	                $v =~ s/\+/ /g; $v =~ s/%(..)/pack("c",hex($1))/ge;
        	        $ret{$k} = $v;
	        }
	}
	return \%ret;
}


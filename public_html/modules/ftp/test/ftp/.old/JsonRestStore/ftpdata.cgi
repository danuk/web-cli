#!/usr/bin/perl

use JSON;
use CGI;
use IO::Socket;
use CGI::Cookie;
use File::Copy;

use strict;

# TODO: check session and IP

my %data;
my @dir;

#open (STDERR, ">>/home/clients/websites/w_vipb/bill.biit.ru/user_sshfs/debug.log") or die $!;

my %mime = ( 	'htm' => 'html','html' => 'html',
		'php' => 'php',	'php3'=> 'php',
		'gif' => 'image','tiff' => 'image','png' => 'image','bmp' => 'image','jpg' => 'image','jpeg' => 'image','ico' => 'image','wmf' => 'image',
		'gz' => 'arch','gzip' => 'arch','zip' => 'arch','tar' => 'arch','tgz' => 'arch','rar' => 'arch',
		'wav' => 'audio','mp3' => 'audio','flac' => 'audio','wma' => 'audio',
		'mpeg' => 'video','mpg' => 'video','avi' => 'video','mkv' => 'video',
		'pdf' => 'pdf',
		'doc' => 'word', # TODO: add all extensions
		'xls' => 'excel', # TODO: add all extensions
		'pl' => 'default', # TODO: add icon
		'cgi' => 'default', # TODO: add icon
	#	'css'	=> 'css',
	#	'js'	=> 'js',
	);


our ($u_fd, $u_filename);

my $cgi = new CGI(\&hook_upload, undef, 0);

my %in = $cgi->Vars;
my $path = $ENV{'PATH_INFO'};

print "Access-Control-Allow-Origin: *\n";

if($ENV{'REQUEST_METHOD'} eq 'OPTIONS')
{
	print $cgi->header(-charset=>'UTF8'); #, 'Access-Control-Allow-Origin' => '*');
	exit 0;
}

#print $cgi->header(-charset=>'UTF8', 'Access-Control-Allow-Origin' => '*');
#print keys %in;

#print "TEST";

my %cookies = fetch CGI::Cookie;
my $session_id = exists $cookies{session_id} ? $cookies{session_id}->value : &clean_sesid($ENV{'QUERY_STRING'}) or die "ERROR: can't get session id $!\n";

die "ERROR: wrong session_id\n" if length $session_id != 32;

$path=~s/^\///;
$path=~s/\.{2,}//; #cut off '..'

chdir "/home/clients/websites/w_vipb/bill.biit.ru/user_sshfs/$session_id" or die $!;
#chdir "/home/clients/websites/w_vipb/";

if ($ENV{'REQUEST_METHOD'} eq 'DELETE')
{
	print $cgi->header(-charset=>'UTF8', -type => 'application/json', -status=> '200');
	
	if (-f $path)
	{
		unlink $path;
	}
	elsif (-d $path)
	{
		rmdir $path;
	}

	exit 0;
}
elsif ($ENV{'REQUEST_METHOD'} eq 'PUT')
{ # rename 
	print $cgi->header(-charset=>'UTF8', -type => 'application/json', -status=> '200');

	my $put = from_json($in{'PUTDATA'});

	my @t = split('/',$path);
	my $dir;
	$dir .= "$t[$_]/" for 0..scalar(@t)-2;
	
	rename "$path", "$dir$put->{name}";

	my $type = $put->{name}; $type=~s/.*\.//;
	my $mime = -d "$dir$put->{name}" ? 'folder' : (exists $mime{$type} ? $mime{$type} : 'default');

	print "{name: \"$put->{name}\", id: \"$dir$put->{name}\", mime: \"$mime\", size: \"0\"}\n";

	exit 0;
}
elsif ($ENV{'REQUEST_METHOD'} eq 'POST' and $in{'uploadedfiles[]'})
{
	print $cgi->header(-charset=>'UTF8', -type => 'application/json', -status=> '200');

	my @filename = $cgi->param('uploadedfiles[]');

	my ($i, @ret);

	for $i (0..scalar(@filename)-1)
	{
		push @ret, {	name => "$filename[$i]", 
				id => "$filename[$i]",
				mime => 'default',	
				size => 0
			};
	}
	print to_json(\@ret) . "\n";

	exit 0;
}


if ($ENV{'REQUEST_METHOD'} eq 'GET' and exists $in{download})
{
	my $file = $in{download}; $file=~s/^.*\///;

	if(-f $in{download})
	{
		my $size = (stat($in{download}))[7];

		print $cgi->header(-charset=>'binary', -type => 'application/octet-stream', -content_length => $size, -attachment => $file);

		# TODO: use sysread or read instead string based output (print)
		open FD, "$in{download}" or die $!;
		while (<FD>)
		{
			print $_;
		}
		close FD;
	}
	elsif (-d $in{download})
	{
		print $cgi->header(-charset=>'binary', -type => 'application/octet-stream', -attachment => "$file.tgz");

		print `/bin/tar -czf - $in{download}`;
	}

	exit 0;
}
elsif ($ENV{'REQUEST_METHOD'} eq 'GET' and $ENV{'CONTENT_TYPE'} eq 'application/x-www-form-urlencoded' and exists $in{get})
{
	print $cgi->header(	-charset=> exists $in{charset} ? $in{charset} : 'UTF8',
	       			-type => 'text/html');

	exit 0 if not -f "$in{get}";

	my $size = (stat($in{get}))[7];
	my $mime = `/usr/bin/file -b --mime-type $in{get}`; chomp($mime);

	if ($mime=~/^text\// or $mime eq 'inode/x-empty')
	{
		if ($size > 1048576) # 1M
		{
			print "Error: file very big: $size\n";
			exit 0;
		}

		open FD, "$in{get}" or die $!;
		while (<FD>)
		{
			print $_;
		}
		close FD;
	}
	else
	{
		print "Error: Not supported file format [$mime]\n";
	}
}
elsif ($ENV{'REQUEST_METHOD'} eq 'GET' and $ENV{'CONTENT_TYPE'} eq 'application/json')
{
	print $cgi->header(-charset=>'UTF8', -type => 'application/json');

	# for symlinks!
	my $cmd;
    
	#$path = $in{id} if $in{id};
	$cmd = $path ? "$path/" : '';
	$cmd = "$in{id}/" if $in{id};

	my @ret = `ls -la $cmd`;

	for (sort {$b cmp $a} @ret)
	{
		my @t = split(/\s+/);
		next if $t[8]!~/\w/;

		my $file;
		$file .= "$t[$_] " for 8..scalar(@t);
		$file =~s/\s+$//;

		my %hash = (name => $file);
		$hash{id} = $path ? "$path/$file" : $file;
		$hash{size} = $t[4];

		my $type = $file; $type=~s/.*\.//;
		$hash{mime} = -d "$hash{id}" ? 'folder' : (exists $mime{$type} ? $mime{$type} : 'default');

		if ($t[0]=~/^[ld]/ and -d $hash{id}) # check symlink 
		{
			$hash{children} = "true";
			$hash{'$ref'} = $hash{id};
		}

		push @dir, \%hash;
	}

	%data = ( children => \@dir );

	print to_json($path ? \%data : \@dir, {pretty => 1}) . "\n";
	#print to_json(\@dir , {pretty => 1}) . "\n";

}

#print $cgi->header(-charset=>'UTF8', -type => 'application/json');
#print "$_ => $ENV{$_}\n" for keys %ENV;

exit 0;

sub hook_upload
{
	my ($filename, $buffer, $bytes_read) = @_;

	if ($filename ne $u_filename)
	{
		close $u_fd if defined $u_fd;

		my $sid = &clean_sesid($ENV{'QUERY_STRING'}) or die "ERROR: can't get session id\n";

		open ($u_fd, ">/home/clients/websites/w_vipb/bill.biit.ru/user_sshfs/$sid/$filename") or die $!;
	
		binmode $u_fd;
		
		$u_filename = $filename;
	}

	print $u_fd $buffer;

	return;
}

sub clean_sesid
{
	my $sesid = shift;

	return $sesid=~/^session_id=(.{32})$/ ? $1 : undef;
}

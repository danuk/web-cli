#!/usr/bin/perl

use JSON;
use CGI;
use IO::Socket;
use CGI::Cookie;
use File::Copy;
use POSIX;

use strict;

# TODO: check session and IP

my $pretty = 0;
my %data;
my @dir;

our $base_dir;
require '../../../conf/config.conf';

open (STDERR, ">>/home/clients/websites/w_vipb/bill.biit.ru/public_html/modules/ftp/ftp.log") or die $!;

my %mime = ( 	'htm' => 'html','html' => 'html', 'shtml' => 'html',
		'php' => 'php',	'php3'=> 'php',
		'gif' => 'image','tiff' => 'image','png' => 'image','bmp' => 'image','jpg' => 'image','jpeg' => 'image','ico' => 'image','wmf' => 'image',
		'gz' => 'arch','gzip' => 'arch','zip' => 'arch','tar' => 'arch','tgz' => 'arch','rar' => 'arch',
		'wav' => 'audio','mp3' => 'audio','flac' => 'audio','wma' => 'audio','flv' => 'audio',
		'mpeg' => 'video','mpg' => 'video','avi' => 'video','mkv' => 'video', 'f4v' => 'video', 'mp4' => 'video', 'mov' => 'video', 'm4a' => 'video', 'mp4v' => 'video', '3gp' => 'video', '3ga' => 'video',
		'pdf' => 'pdf',
		'doc' => 'word', # TODO: add all extensions
		'xls' => 'excel', # TODO: add all extensions
		'pl' => 'default', # TODO: add icon
		'cgi' => 'default', # TODO: add icon
		'css'	=> 'css',
		'js'	=> 'javascript',
		'xml'	=> 'xml',
		'json'	=> 'json',
	);

#my $args = &parse_query_string();
#my $session_id = $args->{session_id}; # for hook_upload

our ($u_fd, $u_filename);
my $cgi = new CGI(\&hook_upload, undef, 0);

print "Access-Control-Allow-Origin: *\n";

if($ENV{'REQUEST_METHOD'} eq 'OPTIONS')
{
	print $cgi->header(-charset=>'UTF8'); #, 'Access-Control-Allow-Origin' => '*');
	exit 0;
}

my %in = $cgi->Vars;

my %cookies = fetch CGI::Cookie;
my $session_id = defined $cookies{session_id} ? $cookies{session_id}->value : (exists $in{session_id} ? $in{session_id} : &parse_query_string()->{session_id});

my $path = exists $in{path} ? $in{path} : &parse_query_string()->{path};
$path=~s/^\///;
$path=~s/\.{2,}//; #cut off '..'
die if $path=~/[&;]/;

#warn "id: [$session_id], path: [$path]\n";

die "ERROR: wrong session_id\n" if length $session_id != 32;

chdir "$base_dir/user_sshfs/$session_id" or die $!;

#print $cgi->header();

for (0..3) { &folder_is_empty("$base_dir/user_sshfs/$session_id") ? sleep 1 : last; }

#chdir "/home/clients/websites/w_vipb/";
#chdir "/home/clients/websites/w_vipb/bill.biit.ru/public_html";

if ($ENV{'REQUEST_METHOD'} eq 'DELETE')
{ # delete
	print $cgi->header(-charset=>'UTF8', -type => 'application/json', -status=> '200');

	my $dir; my $file; my %ret;

	if ($path=~/^(.*)\/(.+)$/) { 
		($dir, $file) = ($1, $2);
	}
	else { 
		print to_json({ status => 'Error: unknown path format' }) . "\n";
		exit 0;
	}

	if (-f $path)
	{	
		%ret = (path => $dir, name => $file, status => 'deleted');
		unlink $path;
	}
	elsif (-d $path)
	{
		%ret = (path => "$dir/$file", name => $file, status => 'deleted');
		`rm -rf $path`;
	}

	print to_json(\%ret) . "\n";

	exit 0;
}
elsif ($ENV{'REQUEST_METHOD'} eq 'PUT')
{ # mkdir, mkfile, rename
	print $cgi->header(-charset=>'UTF8', -type => 'application/json', -status=> '200');

	my %ret;

	if ($in{action} eq 'mkdir')
	{
		my $dir = $in{path}.'/'.$in{name};
		if (mkdir "$dir")
		{
			%ret = ( path=>"$dir", name=> $in{name}, mime => 'folder' );
		}
	}
	elsif ($in{action} eq 'mkfile')
	{
		my $file = "$in{path}".'/'.$in{name};

		exit if -e $file;

		open FD, ">$file" or die $!; close FD;
	
		%ret = ( path=>"$in{path}", name=> $in{name}, mime => &get_mime($file) );
	}
	elsif ($in{action} eq 'rename')
	{
		#my $path = substr($in{path},0, length($in{path})-length($in{name}));
		#$path = '' if $path eq $in{path};
	
		my $path = $in{path};
		my $src;
		my $dst;

		my $src = "$path/$in{name}";
		my $dst = "$path/$in{newname}";

		if ($in{mime} eq 'folder')
		{
			$path = substr($in{path},0, length($in{path})-length($in{name}));
			$src = "$path$in{name}";
			$dst = "$path$in{newname}";
			$path = $dst;
		}
	
		if (rename $src, $dst)
		{
			%ret = ( path=> $path, name=> $in{newname}, mime=> &get_mime($dst) );
		}
	}
	elsif ($in{action} eq 'unpack')
	{
		my $file = "$in{path}/$in{name}";

		chdir $in{path} or die "can't chdir to $in{path}";
		die 'file not exists' if not -f $in{name};
		my $type = `/usr/bin/file -b --mime-type $in{name}`; chomp($type);


		my %arch = ( 'application/zip' => ['/usr/bin/unzip','-u'],
	      
	
		);

		if (not exists $arch{$type}) {
			print to_json( { status => 1, msg => "unsuported format: $type" }) . "\n";
			exit 0;
		}

		my $pid = fork();

		if ($pid == 0)
		{
			POSIX::setsid();
			close (STDOUT);
			close (STDERR);
			exec ( @{$arch{$type}} ,"$in{name}");

			exit 0;
		}

		%ret = ( status => 0, msg => 'OK', file => $file, pid => $pid );
	}
	elsif ($in{action} eq 'check')
	{
		die if $in{pid}!~/^\d+$/;
		my $status = -d "/proc/$in{pid}" ? 1 : 0;

		%ret = ( status => $status, pid => $in{pid} );
	}

	print to_json(\%ret) . "\n";

	exit 0;
}
elsif ($ENV{'REQUEST_METHOD'} eq 'POST' and $in{'uploadedfiles[]'})
{ # upload file[s]
	print $cgi->header(-charset=>'UTF8', -type => 'application/json', -status=> '200');

	my @childrens;
	my %data = (path => "$path", children => \@childrens);

	my @filename = $cgi->param('uploadedfiles[]');

	for (0..scalar(@filename)-1)
	{
		push @childrens, {	path => "$path", 
					name => "$filename[$_]",
					mime => &get_mime($filename[$_]),
				};
	}
	print to_json(\%data, {pretty => $pretty}) . "\n";

	exit 0;
}
elsif ($ENV{'REQUEST_METHOD'} eq 'POST' and $path)
{
	my $charset = &parse_query_string->{charset};

	print $cgi->header(-charset=> 'UTF8', -type => 'application/json', -status=> '200');

	my %ret;

	if (-f "$path")
	{
		my $data = $in{POSTDATA};

		if ($charset)
		{
			use Text::Iconv;
			my $converter = Text::Iconv->new("UTF8", $charset);
			$data = $converter->convert($in{POSTDATA});
		}
	
		open FD, ">$path" or die $!;
		print FD $data;
		close FD;

		$ret{status} = 'OK';
	}
	else
	{
		$ret{status} = 'Error';
	}

	print to_json( \%ret, {pretty => $pretty}) . "\n";

	exit 0;
}
elsif ($ENV{'REQUEST_METHOD'} eq 'GET' and exists $in{download})
{ # download file/directory
	my $file = $in{download}; $file=~s/^.*\///;
	my $mime = 'application/octet-stream';

	$mime = &get_mime($in{download}) if exists $in{mime};

	if(-f $in{download})
	{
		my $size = (stat($in{download}))[7];

		print $cgi->header(-charset=>'binary', -type => $mime, -content_length => $size, -attachment => $file);

		# TODO: use sysread or read instead string based output (print)
		open FD, "$in{download}" or die $!;
		while (<FD>)
		{
			print $_;
		}
		close FD;
	}
	elsif (-d $in{download})
	{
		print $cgi->header(-charset=>'binary', -type => $mime, -attachment => "$file.tgz");
		
		#open(TAR, "-|", "/bin/tar -czf - $in{download}") or die $!;
		#TAR->autoflush(1);
		#print $_ while (<TAR>);
		#close TAR;

		exec ('/bin/tar','-czf','-',"$in{download}");

		#print `/bin/tar -czf - $in{download}`;
	}

	exit 0;
}
elsif ($ENV{'REQUEST_METHOD'} eq 'GET' and $ENV{'CONTENT_TYPE'} eq 'application/x-www-form-urlencoded' and exists $in{get})
{ # show/edit file

	exit 0 if not -f "$in{get}";

	my $size = (stat($in{get}))[7];
	my $mime = `/usr/bin/file -b --mime-type '$in{get}'`; chomp($mime);

	print $cgi->header(	-charset=> exists $in{charset} ? $in{charset} : 'UTF8',
	      			-type => 'text/html', -x_mime => &get_mime($in{get}) , -mime => $mime, -size => $size);

	if ($mime=~/^text\// or $mime eq 'inode/x-empty' or $mime eq 'application/xml' or $mime eq 'inode/symlink')
	{
		if ($size > 1048576) # 1M
		{
			print "Error: file very big: $size\n";
			exit 0;
		}

		open FD, "$in{get}" or die $!;
		while (<FD>)
		{
			print $_;
		}
		close FD;
	}
	else
	{
		print "Error: Not supported file format [$mime]\n";
	}
}
elsif ($ENV{'REQUEST_METHOD'} eq 'GET') # and $ENV{'CONTENT_TYPE'} eq 'application/json')
{ # get tree
	print $cgi->header(-charset=>'UTF8', -type => 'application/json');

	my (@childrens, %data);

	my $idx = 0;

	%data = $path=~/\// ? (path => "$path", children => \@childrens) :
			(identifier => 'id', label => 'name', items => [{id => $idx++, path => "$path", name => $path, mime => 'folder', children => \@childrens}]);

	opendir(my $dh, $path ? "$path" : ".") or die $!;

	for(readdir $dh)
	{
		next if /^\.{1,2}$/;
		
		my %file = (id => $idx++, name => $_, path => ($path ? (-d "$path/$_" ?  "$path/$_" : "$path") : "$_" ) );


		if (-d ($path ? "$path/$file{name}" : $file{name}) )
		{
			$file{children} = [];
			$file{mime} = 'folder';
		}
		else
		{
			$file{mime} = &get_mime($file{name});
		}
		push @childrens, \%file;
	}
	closedir $dh;

	#@childrens = sort {$a->{name} <=> $b->{name}} @childrens;
	@childrens = sort {&up_folders($a->{mime}) <=> &up_folders($b->{mime})} @childrens;
	
	print to_json(\%data, {pretty => $pretty}) . "\n";
}
else
{
	print $cgi->header(-charset=>'UTF8', -type => 'text/html');
	print "Unknown action\n";
}


exit 0;

sub get_mime
{
	my $name = shift;

	#my $mime = `/usr/bin/file -b --mime-type $in{get}`; chomp($mime);

	return 'folder' if -d "$name";

	$name=~s/.*\.//;
	return exists $mime{lc($name)} ? $mime{lc($name)} : 'default';

}

sub up_folders { $_[0] eq 'folder' ? 0 : 1; }

sub hook_upload
{
	my ($filename, $buffer, $bytes_read) = @_;


	if ($filename ne $u_filename)
	{
		my $args = &parse_query_string();
		my $session_id = $args->{session_id};
		die "ERROR: wrong session_id\n" if length $session_id != 32;

		my $path = $args->{path};
		$path=~s/^\///; $path=~s/\.{2,}//; #cut off '/' and '..'

		close $u_fd if defined $u_fd;

		warn "PATH:$base_dir/user_sshfs/$session_id/PATH_VAR:$path/$filename\n";
		open ($u_fd, ">$base_dir/user_sshfs/$session_id/$path/$filename") or die $!;
		
		binmode $u_fd;
	
		$u_filename = $filename;
	}

	print $u_fd $buffer;

	return;
}

sub parse_query_string
{
	my %ret;

	if ($ENV{'QUERY_STRING'}=~/=/)
	{
	        my @in = split(/\&/, $ENV{'QUERY_STRING'});
	
        	my $i;

	        foreach $i (@in)
        	{
	                my ($k, $v) = split(/=/, $i, 2);
        	        $k =~ s/\+/ /g; $k =~ s/%(..)/pack("c",hex($1))/ge;
	                $v =~ s/\+/ /g; $v =~ s/%(..)/pack("c",hex($1))/ge;
        	        $ret{$k} = $v;
	        }
	}
	return \%ret;
}

sub folder_is_empty
{
	my $folder = shift;
	
	opendir (DIR, $folder) or die $1;

	while (readdir(DIR)) { closedir DIR and return 0 if $_ ne '.' and $_ ne '..'; }

	return 1;
}

#!/usr/bin/perl

our %config;

require '../../../conf/config.conf';

use CGI;
my $cgi = new CGI;

print "Accept-Language: ru-RU,ru;q=0.8,en-US;q=0.6,en;q=0.4\n";
print $cgi->header(-charset=>'UTF8', -type=>'text/javascript');

my %in = $cgi->Vars();

my $desc = $in{user_id} . "x" . time();

my $ret = `/usr/bin/curl -s --header 'Accept-Language: ru-RU,ru;q=0.8,en-US;q=0.6,en;q=0.4' 'https://paymaster.ru/ru-RU/widget/Basic/1?LMI_MERCHANT_ID=$config{paymaster_id}&LMI_PAYMENT_AMOUNT=100&LMI_PAYMENT_DESC=$desc&LMI_CURRENCY=RUB'`;

$ret=~s/document\.write/dojo.byId("content").innerHTML=/;
$ret=~s/document\.write\(''\);/''/;
$ret=~s/<form /<form target="_blank" /i;

print $ret;


exit 0;


#!/usr/bin/perl

use strict;

require '../libs/cbi-lib.pm';

use CGI qw(header);
use GD::SecurityImage 1.64; # we need the "blank" style
        
my $font  = "StayPuft.ttf";
	      
my $image = GD::SecurityImage->new(
	width  =>   240,
	height =>    50,
	ptsize =>    20,
	frame  =>     1,
	rndmax =>     1, # keeping this low helps to display short strings
	lines  =>     1,
	thickness  => 2,
	send_ctobg => 1,
	font   =>     '/usr/share/fonts/truetype/ttf-dejavu/DejaVuSans-Bold.ttf',
	scramble =>   1,
	);

	my $word = &passgen(5);
	$image->random( $word );

	# use the blank style, so that nothing will be drawn
	#    # to distort the image.
	
	#$image->create( ttf => 'blank', '#CC8A00' );
	$image->create( ttf => 'circle', '#808080', '#FFFFFF' );
	$image->particle(200, 5);

	#$image->info_text(
	#	text   => 'You are visitor number',
	#	ptsize => 10,
	#	strip  =>  0,
	#	color  => '#0094CC',
	#	);

	umask 0077;
	open FD, ">/tmp/$ENV{REMOTE_ADDR}" or die $!;
	print FD lc($word);
	close FD;

	my($data, $mime, $random) = $image->out;
	
	my $len = length($data);
	binmode STDOUT;
	print "Content-Type: image/png\n";
	print "Content-Length: $len\n";
	print "Cache-Control: no-cache, must-revalidate\n";
	print "Pragma: no-cache\n";
	print "Expires: Sat, 26 Jul 1997 05:00:00 GMT\n\n";

	print $data;

exit 0;


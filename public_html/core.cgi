#!/usr/bin/perl

# CBI 1.0 - Client Billing Interface
# Written by DaNuk (DNk) 23.07.2016

use v5.14;
use CGI::Carp qw(warningsToBrowser fatalsToBrowser);
use utf8;

use LWP::UserAgent;

# core_flag defined for used shm-lib.pm in core mode (not use CGI, not read STDIN)
our $core_flag=1;

require '../libs/cbi-lib.pm';

use locale;
use JSON;
use CGI::Cookie;

our %config;
our $cgi;
our %session_config;
our %allow_without_auth_modules;
our %modules;
my $sudo;

my $server = $config{shm_server};
my $agent = $config{shm_agent};

our $ua = LWP::UserAgent->new;
$ua->agent("CBI/1.0 $agent");

use CGI;
my $cgi = new CGI;
our %in = $cgi->Vars;

our $mod = $in{'mod'};
delete $in{mod};

unless ( $mod ) {
    if ( $ENV{REQUEST_URI} =~/^\/(shm\/.+)\.shtml$/  ) {
        $mod = $1;
    }
}

# fetch existing cookies
our %cookies = fetch CGI::Cookie;
our $session_id = $cookies{session_id};

if ( !$session_id && $in{reg} && $in{user} && $in{pass} ) {
# Register

    #	if (!&check_captcha($in{'captcha'}))
    #	{
    #	    print $cgi->header(-type=>'application/json',-charset=>$config{'charset'});
    #		print to_json( {status => 3, msg => 'Проверочный код введен неверно' } ) . "\n";
    #		exit 0;
    #	}

	my $login = $in{user};
	my $pass = $in{pass};

	if (exists $cookies{ref}) {
		if ($cookies{ref}=~/ref%3D(\d+);/) {
			$in{ref} = $1;
		}
	}

	my $host = exists $in{ref} ? $in{ref} : $ENV{HTTP_HOST};

    my $ret = register_user( $login, $pass );

	print $cgi->header(-type=>'application/json',-charset=>$config{'charset'});
    print to_json( {status => $ret ? 0 : 403, msg => 'Incorrect login' } ) . "\n";

	exit 0;
}
elsif (not defined($session_id) and exists $in{remember} and (exists $in{'user'} or exists $in{'email'}))
{ # Remember
	print $cgi->header(-type=>'application/json',-charset=>$config{'charset'});

	my $login = &encode($in{user});
	my $mail = &encode($in{email});
	my $host = &encode($ENV{HTTP_HOST});

	my @ret;
	my $status;
	my $msg;

	if (length $in{user})
	{
		@ret = `$config{shm_remember_bin} login $login $host`;
	}
	else
	{
		@ret = `$config{shm_remember_bin} mail $mail $host`;
	}

	if ($ret[0]=~/OK/)
	{
		$status = 0;
		$msg = "Пароль будет отправлен на ваш email";
	}
	else
	{
		$status = 1;
		$msg = $ret[0];
	}

	print to_json( {status => $status, msg => $msg } ) . "\n";

	exit 0;
}
elsif (not defined($session_id) and exists $in{'user'} and exists $in{'pass'})
{ # Authorize

	my $user = &trim($in{'user'});
	my $pass = &trim($in{'pass'});

	my $session_id = &validate_user($user, $pass);

	if ($session_id)
	{
		# Create cookies and redirect to
		if (exists $cookies{action})
		{
			my $url = $cookies{action};
			$url=~s/^action=//;
			$url=~s/;\s+.*$//;
			print "Set-Cookie: action=; path=/; expires=Tue, 19-Apr-2011 10:56:56 GMT\n";
			&print_header(&create_cookie('session_id',$session_id), "?$url");
		}
		else
		{
			&print_header(&create_cookie('session_id',$session_id));
		}
	}
	else
	{
		# Incorrect login or password. Redirect to login.shtml?status=1
		&delete_cookie(0,1);
	}
}
elsif (defined($session_id))
{ # Verifuing session and proccess done...
	$session_id = $cookies{'session_id'}->value;

	if ($mod=~/^shm\//)
	{ # skip &legalize_session if shm command (for faster)
		&show_page($mod);
	}
	elsif (&legalize_session($session_id))
	{
		if ( exists $in{'logout'} )
		{ # logout session
		  	my $req = HTTP::Request->new(GET => "$server/user/logout.cgi");
			$req->header( cookie => "session_id=$session_id" );
			$ua->request($req);

			# Redirect to login.shtml?status=2
			&delete_cookie($session_id,2);
		}
		else
		{
			$mod = 'index' if $mod eq 'register'; # not show register page if already authorized
			&show_page($mod);
		}
	}
	else
	{ # delete session (in legalize() function) and cookies this
		# Redirect to login.shtml?status=3
		&delete_cookie($session_id,3);
	}
}
else
{ # Not authorized
	$mod = 'login' if not defined $allow_without_auth_modules{$mod};   # Set default module

	&save_current_cookies();

	&show_page($mod);
}

exit 0;

sub save_current_cookies
{
	if ($ENV{QUERY_STRING}=~/(action|ref)=/)
	{ # Set url cookie if not exist url cookie and not system module
		my $tag = $1;
		my $query = $ENV{QUERY_STRING};
		$query =~s/^mod=\w+//; # not save modules
		print "Set-Cookie:" . &create_cookie($tag, $query) . "\n" if length($query);
	}
}

sub legalize_session
{
	my $session_id = shift || return 0;

  	my $req = HTTP::Request->new(GET => "$server/user/auth.cgi");

	$req->header( cookie => "session_id=$session_id" );

	my $res = $ua->request($req);

	return 1 if $res->is_success;

	return 0;
}

sub register_user {
	my ($user,$pass) = @_;

	return 0 if not defined($user);
	return 0 if not defined($pass);

    my $req = HTTP::Request->new(POST => "$server/user/register.cgi");
	$req->content_type('application/x-www-form-urlencoded');
	$req->content("login=$user&password=$pass");

	my $res = $ua->request($req);

	if ( $res->is_success ) {
        return 1;
    } else {

    }

	return 0;
}

sub validate_user
{
	my ($user,$pass) = @_;

	return 0 if not defined($user);
	return 0 if not defined($pass);

  	my $req = HTTP::Request->new(POST => "$server/user/auth.cgi");
	$req->content_type('application/x-www-form-urlencoded');
	$req->content("login=$user&password=$pass");

	my $res = $ua->request($req);

	if ($res->is_success)
	{
		return from_json($res->content)->{session_id};
	}
	else
	{
		print $cgi->header;
		print $res->content;
		exit 0;
	}


	return 0;
}

sub show_page
{
	my $module = shift || 'index';

	if ($module=~/^shm\//)
	{
		$module =~s/^shm\///;

		$in{range} = $ENV{HTTP_RANGE} if exists $ENV{HTTP_RANGE};

		my $query = join('&', map("$_=$in{$_}", keys %in));

		my $req;

		if ($ENV{REQUEST_METHOD} eq 'GET')
		{
			$req = HTTP::Request->new(GET => "$server/cbi/$module.cgi?$query");
			$req->header( cookie => "session_id=$session_id" );
		}
		elsif ($ENV{REQUEST_METHOD} eq 'POST')
		{
			$req = HTTP::Request->new(POST => "$server/cbi/$module.cgi");
			$req->header( cookie => "session_id=$session_id" );

			$req->content_type('application/x-www-form-urlencoded');
			$req->content($query);
		}
		else
		{
			print $cgi->header(-status => 405);
			print '[ { "status" : "405", "msg" : "Method not allowed" } ]';
			exit 0;
		}


 		my $res = $ua->request($req);

		printf ("Content-Type: %s\n", $res->headers->{'content-type'});
		printf ("Content-Range: %s\n", $res->headers->{'content-range'}) if exists $res->headers->{'content-range'};

		if (!$res->is_success)
		{
			my $status = $res->status_line;
			$status =~s/\D+//;
			printf ("status: %d\n", $status);

		}
		print "\n";

		print $res->content;

	}
	elsif ($module=~/^appslist$/)
	{
		print $cgi->header(-type=>'application/json',-charset=>$config{'charset'});

		my (@apps, $name, $ver);

		for $name (`ls $config{apps_path}`)
		{
			chomp $name;
			for $ver (`ls "$config{apps_path}/$name"`)
			{
				next if $ver=~/-lock$/;
				chomp $ver;
				push @apps, {name => $name, label => "$name ($ver)", version => $ver, path => "$name/$ver" };
			}
		}
		print to_json( \@apps ) . "\n";
		exit 0;
	}
	elsif ($module=~/^makecs$/)
	{
		print $cgi->header(-type=>'application/json',-charset=>$config{'charset'});

		my @ret = &send_command("shm services settings get --usi='$in{usi}'");
		my $data = from_json(join('',@ret));

		my $domain = $in{domain};
		my $dir = "$in{directory}/public_html";

		for (keys %ENV) { delete $ENV{$_} };
		print `./sitepro.php domain=$domain server=$data->{server}->{server_name} login=$data->{login} password=$data->{password} dir=$dir 2>/dev/null`;

		exit 0;
	}
	elsif ($module=~/^appinstall$/)
	{
		print $cgi->header(-type=>'application/json',-charset=>$config{'charset'});

		$in{app} =~ s/%(..)/pack("c",hex($1))/ge;
		$in{dir} =~ s/%(..)/pack("c",hex($1))/ge;

		if ($in{app}=~/\.\./) { exit 0 };
		if ($in{app}!~/^[A-Za-z0-9\/\.]+$/) { exit 0 };
		if (not -d "$config{apps_path}/$in{app}") { exit 0 };

		chdir "$config{apps_path}/$in{app}" or die;
		die if not -f 'install.pl';

		die if $in{usi}!~/^\d+$/;
		my $path = "$config{sshfs_path}/$session_id/w_$in{usi}";

		die if $in{dir}=~/\.\./;
		die if $in{dir}=~/[~;]/;
		my $dir = "$in{domain_dir}/public_html/$in{dir}";

		for ($in{db_user},$in{db_password},$in{db_name},$in{db_host},$in{db_port},$in{app},$in{domain_id},$in{domain_dir},$in{domain}) { exit 0 if (not defined $_ or $_=~/[&;"']/ )};

		my $home = "/home/clients/websites/w_$in{usi}";

		#print `./install.pl --home=$home --db_user=$in{db_user} --db_password=$in{db_password} --db_name=$in{db_name} --db_host=$in{db_host} --db_port=$in{db_port} --path=$path --dir=$dir`;

		my @cmd = ('install.pl',"--home=$home", "--db_user=$in{db_user}", "--db_password=$in{db_password}", "--db_name=$in{db_name}", "--db_host=$in{db_host}", "--db_port=$in{db_port}", "--path=$path", "--dir=$dir");

		$ENV{SHM_SOCK} = $session_id;

		my ($ret_code,$stdout,$stderr) = &exec_module(\@cmd,undef);

		#print "RET_CODE: $ret_code, OUT: $stdout, Err: $stderr\n";

		if ($ret_code != 0)
		{
			print to_json( {status => 'ERROR', stdout => $stdout, stderr => $stderr } ) . "\n";
			exit 0;
		}

		my $ret_data = from_json($stdout);
		my $app_name = $in{app};
		$app_name=~s/\/.*$//;

		my %t = (domain => "$in{domain}", domain_dir => "$in{domain_dir}", dir => "$in{dir}", db_name => "$in{db_name}", db_user => "$in{db_user}" );
		$t{login} = $ret_data->{login} if exists $ret_data->{login};
		$t{password} = $ret_data->{password} if exists $ret_data->{password};

		my $data = to_json( \%t );

		&send_command("shm apps add --name='$app_name' --usi='$in{usi}' --domain_id='$in{domain_id}' --data='$data' ");

		print to_json( {status => 'OK', data => $ret_data } ) . "\n";

		exit 0;
	}
	elsif ($module=~/^appremove$/)
	{
		print $cgi->header(-type=>'application/json',-charset=>$config{'charset'});

		die if $in{usi}!~/^\d+$/;

		my $path = "$config{sshfs_path}/$session_id/w_$in{usi}";
		chdir "$path" or die;

		my $dir = "$in{domain_dir}/public_html/$in{dir}";
		die if $dir=~/\.\./;
		die if $dir=~/[~;]/;

		if ( not -d "$dir") { print to_json( {status => 'OK', dir => $dir } ) . "\n" };
		die if ! -d $dir;

		$dir .= "*" if $dir=~/\/public_html\/$/; # not delete public_html folder
		`/bin/rm -rf $dir`;

		print to_json( {status => 'OK' } ) . "\n";

		exit 0;
	}
	elsif ($module=~/^startftp$/)
	{
		print $cgi->header(-type=>'application/json',-charset=>$config{'charset'});

		my @ret = &send_command("shm services settings get --usi='$in{usi}'");
		my $data = from_json(join('',@ret));

		#print @ret; exit 0;
		#print join("\n",@ret);

		print to_json( {status => &start_ftp($session_id, $data)==1 ? 'OK' : 'ERROR' } ) . "\n";

		exit 0;
	}
	elsif ($module=~/^startmysql$/)
	{
		print $cgi->header(-type=>'application/json',-charset=>$config{'charset'});

		my @ret = &send_command("shm services settings get --usi='$in{usi}'");
		my $data = from_json(join('',@ret));

		print to_json( {status => &start_mysql($session_id, $data) ? 'OK' : 'ERROR' } ) . "\n";

		exit 0;
	}
	else
	{
		if (-f "modules/$module")
		{
			&print_header(undef);

			open FD, "<modules/$module" or die $!;

			while (<FD>)
			{
				s/@!--\$(\w+)--!@/$config{$1}/g;
				print $_;
			}
			close FD;
		}
		else
		{
			print $cgi->header(-status=>404);
			print "Error: module not found: $module\n";
		}
	}
	return 0;
}

exit 0;



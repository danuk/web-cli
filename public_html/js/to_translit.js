function to_translit( s )
{
	s = s.replace(/^\s*ИП\s+/i, "");
	s = s.replace(/^\s+/, "");
	s = s.replace(/\s+$/, "");
	s = s.replace(/\s+/g, " ");
	s2 = s.split(/\s+/);

	result = "";

	if (s2.length == 3)
	{
		result = wtt2(s2[1])
			+ " " + wtt2(s2[2].charAt(0))
			+ " " + wtt2(s2[0]);
	}
	else
	{
		result=wtt2(s2[0]);
		for(i=1;i<s2.length;i++){
			result+=" "+wtt2(s2[i]);
		}
	}
	return result;
}

function wtt2( s )
{


	from	= "абвгдзийклмнопрстуфыэ";
	to		= "abvgdziyklmnoprstufye";
	dbl		= {
		'ж' : 'zh',
		'х' : 'kh',
		'ц' : 'ts',
		'ч' : 'ch',
		'ш' : 'sh',
		'щ' : 'shch',
		'ю' : 'yu',
		'я' : 'ya'
	};
	excl		={
		'юрий' : 'Yuri',
//		'юлия' : 'Julia',
		'максим' : 'Maxim',
		'оксана' : 'Oxana',
		'алексей' : 'Alexey',
		'александр' : 'Alexander',
		'александра' : 'Alexandra',
		'людмила' : 'Liudmila',
		'вячеслав' : 'Viatcheslav',
		'акулина':'Akoulina',
		'анатолий':'Anatoly',
		'анисий':'Anisy',
		'арсентий':'Arsenty',
		'афанасий':'Afanasy',
		'василий':'Vasily',
		'виталий':'Vitaly',
		'геннадий':'Gennady',
		'георгий':'Georgy',
		'григорий':'Grigory',
		'дмитрий':'Dmitry',
		'зиновий':'Zinovy',
		'илья':'Ilya',
		'клементий':'Klementy',
		'лаврентий':'Lavrenty',
		'леонтий':'Leonty',
		'любовь':'Liubov',
		'надежда':'Nadezda',
		'филипп':'Philipp'
	};
	substr		={
		'алекс' : 'alex',
		'виктор' : 'victor',
		'максим' : 'maxim',
		'яков' : 'iakov'
	};
	s=s.toLowerCase();	
	if(excl[s]){
		return excl[s];
	}else{
	 	for(i in substr){
			s=s.replace(i,substr[i]);
		}
	}
	// а также:
	// Е,Ё = УE после гласных и Ь,Ъ и в начале слова
	// Е,Ё = E в ост случаях
	
	s = s.replace( /([аеёиоуыюяьъ])[её]/g, "$1ye");
//	s = s.replace( /^[её]/g, "Ye");
	s = s.replace( /[её]/g, "e");
//	s = s.replace( /[иы][й]/g, "y");
	s = s.replace( /[ь][я]/g, "ia");
	s = s.replace( /[и][я]$/g, "ia");
	s = s.replace( /[ьъ]/g, "");
	
	//transliterating
	tmp = "";
	for ( i=0; i<s.length; i++)
	{
	   c = s.charAt(i);
	   k = from.indexOf(c);
	   if ( k != -1 )
	   {
	   	   // простые буквы
	   	   tmp += to.charAt(k);
	   }
	   else
	   {
	   	   if ( dbl[c] )
	   	   {
	   	   	   // двойные символы
	   	   	   tmp += dbl[c];
	   	   }
	   	   else 
	   	   {
	   	   	   tmp += c;
	   	   }
	   }
	}
	tmp=tmp.charAt(0).toUpperCase()+tmp.substring(1,tmp.length);
	return tmp;
}	


require([
	"dojo/_base/array", "dojo/_base/fx", "dojo/_base/window", "dojo/dom", "dojo/dom-class", "dojo/dom-geometry", "dojo/dom-style",

	"dojo/hccss", "dojo/date/locale", "dojo/parser", "dojo/store/Memory",
	"dijit/registry", "dijit/tree/ObjectStoreModel",

	"dijit/MenuItem", "dijit/CheckedMenuItem", "dijit/MenuSeparator", "dijit/DropDownMenu",

	// Editors used by InlineEditBox.  Must be pre-loaded.
	"dijit/form/Textarea", "dijit/form/DateTextBox", "dijit/form/TimeTextBox", "dijit/form/FilteringSelect",

	// Modules referenced by the parser
	"dijit/Menu", "dijit/PopupMenuItem", "dijit/ColorPalette", "dijit/layout/BorderContainer", "dijit/MenuBar",
	"dijit/PopupMenuBarItem", "dijit/layout/AccordionContainer", "dijit/layout/ContentPane", "dijit/TooltipDialog",
	"dijit/Tree","dijit/layout/TabContainer", "dijit/form/ComboButton", "dijit/form/DropDownButton", "dijit/form/ToggleButton",
	"dijit/form/CheckBox", "dijit/form/RadioButton", "dijit/form/CurrencyTextBox", "dijit/form/NumberSpinner",
	"dijit/form/Select", "dijit/Editor", "dijit/form/VerticalSlider", "dijit/form/VerticalRuleLabels",
	"dijit/form/VerticalRule", "dijit/form/HorizontalSlider", "dijit/form/HorizontalRuleLabels",
	"dijit/form/HorizontalRule", "dijit/TitlePane", "dijit/ProgressBar", "dijit/InlineEditBox", "dojo/dnd/Source",
	"dijit/Dialog",

	 // Data Grid
	"dojo/query",
 	"dojox/grid/DataGrid", "dojox/grid/_CheckBoxSelector", "dojox/grid/cells", "dojox/grid/cells/dijit",
        "dojo/store/Cache", "dojo/data/ObjectStore", "dojo/store/JsonRest","dojox/layout/ExpandoPane",

	// Don't call the parser until the DOM has finished loading
	"dojo/domReady!",
	"dojo/ready",
], function(array, baseFx, win, dom, domClass, domGeom, domStyle, has, locale, parser, Memory, registry, ObjectStoreModel,
			Menu, MenuItem, Select, domFormm, DropDownButton, DropDownMenu, request, json){

		parser.parse(dom.byId('container')).then(function(){
			dom.byId('loaderInner').innerHTML += " OK.";
			setTimeout(function hideLoader(){
				baseFx.fadeOut({
					node: 'loader',
					duration: 500,
					onEnd: function(n){
						n.style.display = "none";
					}
				}).play();
			}, 250);

			registry.byId('Menu1').on("ItemClick", function(node){
				if (node.id != 'documents') ShowModule(node.label, 'menu/'+node.id, null, 'dlg_'+node.id)

			});
			registry.byId('documentsMenu').on("ItemClick", function(node){
				if (node.id != 'order' ) ShowModule(node.label, 'menu/'+node.id, null, "dlg_"+node.id )
			});

			registry.byId("order").on("Click", function(){ ShowModule(this.label, '/menu/order','width:300px','dlg_order') });

			registry.byId('Menu2').on("ItemClick", function(node){
				if (node.id != 'register') { ShowModule(node.label, 'menu/'+node.id, null, "dlg_"+node.id) }
			});

			registry.byId("menu_passwords").on("Click", function(){ ShowModule(this.label, '/menu/passwords',null,'dlg_passwords') });

			dojo.query('#tree').connect("onclick", function() {  ShowModule(this.label, 'tree',null,'tree') } );

			registry.byId("reg_hosting").on("Click", function(){
				reg_hosting();
			});

			registry.byId("reg_domains").on("Click", function(){
				var tabDomains = registry.byId("tabDomains");
				tabDomains.set("RegisterDomain",1);
				registry.byId('tab').selectChild(tabDomains);
			});

			if (registry.byId("menu_tikets")) registry.byId("menu_tikets").on("Click", function(){ ShowModule(this.label, '/tikets/index',null,'dlg_tikets') });
			registry.byId("menu_myclients").on("Click", function(){ ShowModule(this.label, '/partners/list',null,'dlg_myclients') });
			registry.byId("menu_rewardslist").on("Click", function(){ ShowModule(this.label, '/partners/rewards','width:900px','dlg_rewardslist') });
			registry.byId("menu_change_pass").on("Click", function(){ ShowModule('Сменить пароль личного кабинета', 'menu/change_pass', 'width:440px', 'dlg_change_pass')});


	});



});

require([
		"dojo/ready",
		"dijit/registry",
		"dojo/request",
		"dojo/dom",
		"dojo/dom-construct",
		"dojo/dom-style",
		"dojo/aspect",
		"dojo/json",
		"dojo/query",
		"dojo/store/Memory",
		"dojo/store/Observable",
		"dijit/Tree",
		"dijit/tree/ObjectStoreModel",
		"dijit/layout/AccordionContainer",
		"dojox/layout/ContentPane",
		"dojo/cookie",
		"dojox/timing",
], function(ready, registry, request, dom, domConstruct, domStyle, aspect, json, query, Memory, Observable, Tree, ObjectStoreModel, AccordionContainer, ContentPane, cookie){
ready(function(){

	/*t = new dojox.timing.Timer(30000); // 30 sec (1*30*1000)
	t.onTick = function(){ request.get("/shm/user/auth.cgi" ).then(function(ret){ if (ret != 'OK'){ window.location = "/?logout=1" }})};
	t.start();*/

	var GlobalData;

	SetGlobalData = function(obj){ GlobalData = obj };
	GetGlobalData = function(){ return GlobalData };

	parse_args = function() { return dojo.queryToObject(decodeURIComponent((dojo.doc.location.search.slice(1)))) };

	reg_hosting = function() {
		registry.byId('tab').selectChild(registry.byId("tabHosting"));

		hosting_page.set("href", "/modules/hosting/constructor");
		//hosting_page.set("href", "/modules/hosting/tariff");
	}

	passgen = function(){
		var passwd = '';
		var chars = 'abdefihkmnpqrtuxyzABDEFHKLMNPQRTUXYZ123456789';
		for (i=1;i<8;i++) {
			var c = Math.floor(Math.random()*chars.length + 1);
			passwd += chars.charAt(c)
	  	}
		return passwd;
	}

	getusi = function(){ return dijit.byId('tab').selectedChildWidget.id.match(/\d+/)[0] }

	GetDate = function(date){
		if (date=='') return '';
		var t = new Date(date*1000);
		var day = t.getDate().toString().replace(/^([0-9])$/,'0$1');
		var month = (t.getMonth()+1).toString().replace(/^([0-9])$/,'0$1');
		var year = t.getFullYear();
		return  day + '-' + month + '-' + year;
	}

	GetDateFull = function(date){
		if (date=='') return '';
		var t = new Date(date*1000);
		var day = t.getDate().toString().replace(/^([0-9])$/,'0$1');
		var month = (t.getMonth()+1).toString().replace(/^([0-9])$/,'0$1');
		var year = t.getFullYear();
		var hour = t.getHours().toString().replace(/^([0-9])$/,'0$1');
		var min  = t.getMinutes().toString().replace(/^([0-9])$/,'0$1');
		var sec  = t.getSeconds().toString().replace(/^([0-9])$/,'0$1');
		return  day + '-' + month + '-' + year + ' ' + hour + ':' + min + ':' + sec;
	}


	LoadUser = function(callback){
		request.get("/shm/user/data.shtml", {handleAs: 'json'}).then(function(data){
			for(var obj in data){  var f=dom.byId(obj); if (f){f.innerHTML = data[obj]}}
			if (typeof(callback) !== 'undefined' ) callback();

			// refresh grdDomains if selected tabDomains
			if (registry.byId('tab').selectedChildWidget == registry.byId('tabDomains')) registry.byId("grdDomains")._refresh();
		});
	}

	LoadUser();

	dojo.connect( dom.byId('balance_refresh') , "onclick", function () { LoadUser() } );


	var dlg_cnt = 0;

	ShowModule = function(title,module,style,id){

		if (!style) style = "width:900px;";

		var myPane = new dojox.layout.ContentPane({
				href: 'modules/' + module,
		    	ioArgs: {timeout: 5000},
				executeScripts: true,
		});

		dlg_cnt++;

		var dialog_id = typeof(id)!=='undefined' ? id :  "myDialog"+dlg_cnt;

		var myDialog = new dijit.Dialog({
				id: dialog_id,
				title: title,
				hide: function(){ if (typeof(player)!=='undefined') {player.destroy()}; this.destroyRecursive() },
				style: style
		});

		myDialog.addChild(myPane);
		myDialog.show();

		var dlg = dojo.coords(myDialog.id);
		var dlgTop = Math.round(dlg.h/2) + 'px';
		dojo.style(myDialog.containerNode.parentNode,{ top: dlgTop, visibility:'visible' });

		return myDialog;
	}

	// Parse URI
	uri = parse_args();

	if (uri.module) { ShowModule(uri.module,uri.module) };


	// Quick panel
	load_main_panel = function() {

		var menu_tariff = registry.byId("menu_tariff_list");
		var menu_sites = registry.byId("menu_sites_list");
		var menu_mail = registry.byId("menu_mail_list");
		var menu_sql = registry.byId("menu_sql_list");
		var menu_dns = registry.byId("menu_dns_list");
		var menu_ssh = registry.byId("menu_ssh_list");
		var menu_ftp = registry.byId("menu_ftp_list");
		var menu_cs = registry.byId("menu_cs_list");

		menu_tariff.getChildren().forEach(function(child){ child.destroy() });
		menu_sites.getChildren().forEach(function(child){ child.destroy() });
		menu_mail.getChildren().forEach(function(child){ child.destroy() });
		menu_sql.getChildren().forEach(function(child){ child.destroy() });
		menu_dns.getChildren().forEach(function(child){ child.destroy() });
		menu_ssh.getChildren().forEach(function(child){ child.destroy() });
		menu_ftp.getChildren().forEach(function(child){ child.destroy() });
		menu_cs.getChildren().forEach(function(child){ child.destroy() });

		request.get("/shm/services/category.shtml", {handleAs: 'json'}).then(function(data){

		for(var root in data){
			if (data[root].category.match(/^web_(tariff|virt)|dedicated_hostin|vps$/)){

				var tariff_label =  ' [' + root + '# '+ data[root].name + ']';

				// TARIFF
				var menuItem0 = new dijit.MenuItem({
					id: 'hosting_tariff_'+root,
				    	usi: root,
					label: tariff_label,iconClass:"menu_tariffIcon",onClick: function(){
						hosting_page.set("tariff_id", this.usi );
						hosting_page.set("href", "/modules/hosting/index");

				}});
				menu_tariff.addChild(menuItem0);

				// WEB
				if ( data[root].services && data[root].services.web ){

					for (web_service_id in data[root].services.web  ) {
						var web = data[root].services.web[web_service_id];

						if (web.domains) {
							var web_label =  web.settings.login + tariff_label;
							var domains = web.domains;
							dojo.forEach(domains, function(domain){
								menu_sites.addChild(
									new dijit.MenuItem({
										usi: root,
										web_service_id : web_service_id,
										label: domain.domain + tariff_label,
										    	iconClass:"menu_mysitesIcon",onClick: function(){
												hosting_page.set("tariff_id", this.usi );
												hosting_page.set("web_service_id", this.web_service_id );
												hosting_page.set("href", "/modules/hosting/index");
											}
									})
								)
								MakeDNS(domain);
								MakeCS(web.user_service_id, domain);
							})
							registry.byId("menu_sites").set("disabled",false);

							// FTP
							var menuItem2 = new dijit.MenuItem({
								usi: web,
								label: web_label,iconClass:"menu_ftpIcon",onClick: function(){ MakeFTP(this.usi) }});

							// SSH
							var menuItem1 = new dijit.MenuItem({
								usi: web,
								label: web_label,iconClass:"menu_sshIcon",onClick: function(){ MakeSSH(this.usi) }});

							menu_ssh.addChild(menuItem1);
							menu_ftp.addChild(menuItem2);

							registry.byId("menu_ftp").set("disabled",false);
							registry.byId("menu_ssh").set("disabled",false);
							registry.byId("menu_cs").set("disabled",false);
						}
					}

				}

				// MAIL
				if (data[root].services && data[root].services.mail && data[root].services.mail.domains){
					var domains = data[root].services.mail.domains;
					dojo.forEach(domains, function(domain){
						menu_mail.addChild(
							new dijit.MenuItem({
							usi: root,
							label: domain.domain + tariff_label,
							    	iconClass:"menu_mailIcon",onClick: function(){
									MakeMail(this.usi,domain);
								}
							})
						)
						MakeDNS(domain);
					})
					registry.byId("menu_mail").set("disabled",false);
				}

                // MySQL
				if (data[root].services && data[root].services.mysql){

					for (mysql_service_id in data[root].services.mysql) {
						var mysql = data[root].services.mysql[mysql_service_id];
						if (!mysql.server) continue;
						var mysql_server_name = mysql.server.server_name;
						var mysql_server_port = mysql.settings.port;

						var menuItem3 = new dijit.MenuItem({
							usi: mysql,
							label: mysql_server_name + ':' + mysql_server_port + ' ' + tariff_label,
							iconClass:"menu_databasesIcon",onClick: function(){ MakeMySQL(this.usi) }});

						menu_sql.addChild(menuItem3);
						registry.byId("menu_sql").set("disabled",false);
					}
				}
			}
		}

		if (hosting_page.get('tariff_id'))
		{	// show tariff selected
			hosting_page.set("href", "/modules/hosting/index");
		}
		else if (uri.action==null && menu_tariff.getChildren().length) {
			// show last tariff
			var last_tariff_id =  menu_tariff.getChildren().length - 1;
			hosting_page.set("tariff_id", menu_tariff.getChildren()[last_tariff_id].id.match(/\d+/)[0] );
			hosting_page.set("href", "/modules/hosting/index");
		}
		else { // Show create tariff page
			hosting_page.set("href", "/modules/hosting/constructor");
		}

		menu_tariff.addChild(
			new dijit.MenuItem({label: "Создать новый тариф", iconClass:"add_tariff_Icon",onClick: function(){
				hosting_page.set("tariff_id", "");
				hosting_page.set("href", "/modules/hosting/constructor");
			}})
		);

		if (menu_dns.getChildren().length) {
			registry.byId("menu_dns").set("disabled",false);
		}

	}) // end request,get section
	}; // end load_main_panel function

	load_main_panel();

	tohex = function(str) { var hex = ''; for(var i=0;i<str.length;i++) hex += ''+str.charCodeAt(i).toString(16); return hex }

	MakeSSH = function(web){
			tab = registry.byId('tab');

			var cp = registry.byId('tab_ssh_'+ web.user_service_id);

			if (!cp)
			{
				var login = tohex(web.settings.login);
				var password = tohex(web.settings.password);
				var server_id = web.server.server_name.match(/\d+/);

				cp = new ContentPane({
					title: 'SSH: ' + web.settings.login,
			    	id: 'tab_ssh_'+ web.user_service_id,
					iconClass: "menu_sshIcon",
			    	closable: true,
					content: '<iframe height="99%" width="99%" src='+config.ssh_url+'?login='+login+'0a&password='+password+'0a&srv='+server_id+'&sid='+cookie('session_id')+'"></iframe>'
				});
				tab.addChild(cp);

			}
			tab.selectChild(cp);
	}

	MakeFTP = function(web){
			tab = registry.byId('tab');
			var cp = registry.byId('tab_ftp_' + web.user_service_id);

			if (!cp) {
				cp = new ContentPane({
					title: 'FTP: ' + web.settings.login,
			    	//id: 'tab_ftp_' + web.user_service_id,
					id: 'tab_ftp_' + web.user_service_id,
					folder: web.settings.login,
					iconClass: "menu_ftpIcon",
		    		closable: true,
					href: 'ftp/index.shtml',
				});
				tab.addChild(cp);
			}
			tab.selectChild(cp);
	}

	MakePHP = function(web){
			tab = registry.byId('tab');
			var cp = registry.byId('tab_php_' + web.user_service_id);

			if (!cp) {
				cp = new ContentPane({
					title: 'PHP: ' + web.settings.login,
					id: 'tab_php_' + web.user_service_id,
					closable: true,
					href: 'hosting/php_config.shtml',
				});
				tab.addChild(cp);
			}
			tab.selectChild(cp);
	}

	MakeMySQL = function(mysql){
			var mysql_server_name = mysql.server.server_name;
			var mysql_server_port = mysql.settings.port;

			request.post('startmysql.shtml?usi='+mysql.user_service_id, {handleAs:'json'}).then(function(data){
				if (data.status == 'OK') {
					tab = registry.byId('tab');

					var cp = registry.byId('tab_sql_' + mysql.user_service_id);

					if (!cp) {
						cp = new dojox.layout.ContentPane({
							title: 'MySQL: ' + mysql_server_name + ':' + mysql_server_port,
					    	id: 'tab_sql_' + mysql.user_service_id,
							data: mysql,
							iconClass: "menu_databasesIcon",
							closable: true,
							executeScripts: true,
						});
						tab.addChild(cp);
					}
					tab.selectChild(cp);
					cp.setHref("mysql/index.shtml");
				}
			});
	}

	MakeMail = function(usi,domain){
					var tab = registry.byId('tab');
					var cp = registry.byId('tab_mail_'+ domain.user_service_id);

					if (!cp) {
						cp = new dojox.layout.ContentPane({
							title: 'Почта: ' + domain.domain,
					    		id: 'tab_mail_'+ domain.user_service_id,
							iconClass: "menu_mailIcon",
		    					closable: true,
							executeScripts: true,
							href: 'mail/index.shtml'
						});
						tab.addChild(cp);
					}
					tab.selectChild(cp);
	}

	MakeDNS = function(domain){
			var menu_dns = registry.byId("menu_dns_list");

			var item = null;

			menu_dns.getChildren().forEach(function(child){ if (child.label === domain.domain) item = child });
			if (item) return item;

			var menuItem = new dijit.MenuItem({
				label: domain.domain,
				iconClass:"menu_dnsIcon",
				onClick: function(){
 					var tab = registry.byId('tab');
					var cp = registry.byId('tab_dns_'+ domain.domain_id);

					if (!cp) {
						cp = new dojox.layout.ContentPane({
							title: 'DNS: ' + domain.domain,
					    		id: 'tab_dns_'+ domain.domain_id,
							iconClass: "menu_dnsIcon",
		    					closable: true,
							executeScripts: true,
							href: 'dns/index.shtml'
						});
						tab.addChild(cp);
					}
					tab.selectChild(cp);
				}
			});
			menu_dns.addChild(menuItem);

			return menuItem;
	}

	MakeCS = function(usi,domain){
			var menu_cs = registry.byId("menu_cs_list");

			var item = null;

			menu_cs.getChildren().forEach(function(child){ if (child.label === domain.domain) item = child });
			if (item) return item;

			var menuItem = new dijit.MenuItem({
				label: domain.domain,
				iconClass:"menu_csIcon",
				onClick: function(){
					var new_window = window.open();
					request.post('makecs.shtml', {
						handleAs:'text',
						data: {
							directory: domain.directory,
							domain: domain.domain,
							usi: usi,
						}
					}).then(function(ret){
						new_window.location = ret;
					});
				}
			});
			menu_cs.addChild(menuItem);

			return menuItem;
	}

	ShowAlert = function(msg, callback) {
		var dlgWait = new dijit.Dialog({
			closable: true,
			style: "width:350px",
			title: "Ошибка",
			content: '<img style="float:left;margin-left:10px;margin-right:20px;" src="/img/alert/warning.gif"/><div style="margin-top:5px;">'+msg+'</div>',
		});

		var dlg_btn = new dijit.form.Button({
			style: "clear:both; margin-top: 25px; margin-left:60px;",
			label: "Закрыть",
			onClick: function(){ if (callback) {callback()}; dlgWait.hide() }
		});
		dlgWait.addChild(dlg_btn);
		dlgWait.show();
	};

	ShowInfo = function(msg) {
		var dlgWait = new dijit.Dialog({
			closable: true,
			style: "width:350px",
			title: "Информация",
			content: '<img style="float:left;margin-left:10px;margin-right:20px;" src="/img/alert/info.gif"/><div style="margin-top:5px;">'+msg+'</div>',
		});

		var dlg_btn = new dijit.form.Button({
			style: "clear:both; margin-top: 25px; margin-left:120px;",
			label: "Закрыть",
			onClick: function(){ dlgWait.hide() }
		});
		dlgWait.addChild(dlg_btn);
		dlgWait.show();
	};

	ShowConfirm = function(msg, callback) {
		var dlgWait = new dijit.Dialog({
			closable: true,
			style: "width:350px",
			title: "Подтверждение",
			content: '<img style="float:left;margin-left:10px;margin-right:20px;" src="/img/alert/question.gif"/><div style="margin-top:5px;">'+msg+'</div>',
		});

		var dlg_btn_ok = new dijit.form.Button({
			style: "clear:both; margin-top: 25px; margin-left:10px;",
			label: "Да",
			onClick: function() { callback(); dlgWait.hide() }
		});
		dlgWait.addChild(dlg_btn_ok);

		var dlg_btn_cancel = new dijit.form.Button({
			style: "clear:both; margin-top: 25px; margin-left:80px;",
			label: "Отмена",
			onClick: function(){ dlgWait.hide() }
		});
		dlgWait.addChild(dlg_btn_ok);
		dlgWait.addChild(dlg_btn_cancel);

		dlgWait.show();
	};

	ShowProgress = function(data, func_check, callback ) {
		var dlgWait = new dijit.Dialog({
			closable: false,
			style: "width:320px;",
			title: data.title,
			content: data.msg,
		});

		var prgbar = new dijit.ProgressBar({
			style: "width:300px",
			maximum: data.max,
		});

		dlgWait.addChild(prgbar);
		dlgWait.show();

		var i = 0;

		var go_progress = function(){
			if (func_check()) i = data.max;

			prgbar.set({value: ++i});
			if(i < data.max){ setTimeout(go_progress, 1000) } else {
				callback();
				setTimeout( function() { dlgWait.hide() } , 1000)
			}
		}
		go_progress();
	};

	ShowWait = function(data ) {
		var dlgWait = new dijit.Dialog({
			closable: true,
			style: "width:320px;",
			title: data.title,
			content: data.msg,
		});

		var prgbar = new dijit.ProgressBar({
			style: "width:300px",
			indeterminate: true,
		});
		dlgWait.addChild(prgbar);
		dlgWait.show();

		return dlgWait;
	};

})
});



